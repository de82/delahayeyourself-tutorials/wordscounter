"""
    The ``strings`` module
    ======================

    Use it to import obvious functions about strings.

    :Example:

    >>> from words.strings import count_words
    >>> count_words("Hello There!")
    2
"""


def check_string(string):
    """
    Raise dummy exception if given string is None

    :param string: given string to check

    :Example:

    >>> check_string("string")
    """
    if string is None:
        raise Exception("String must not be empty")


def count_words(string):
    """
    Obviously count words in given string

    :param string: given string to count words in
    :return: Total words in given string
    :rtype: int

    :Example:

    >>> count_words("string")
    1
    >>> count_words("")
    0
    >>> count_words("Hello There!")
    2
    """
    check_string(string)
    if len(string) == 0:
        return 0
    return len(string.split(" "))


def count_chars(string):
    """
    Obviously count characters in given string

    :param string: given string to count characters in
    :return: Total characters in given string
    :rtype: int

    :Example:

    >>> count_chars("string")
    6
    >>> count_chars("")
    0
    >>> count_chars("Hello There!")
    12
    """
    check_string(string)
    return len(string)
