"""
    The ``wordscounter`` module
    ======================

    This is the world famous words counter app made with Python3 and tkinter lib.
"""
from tkinter import Button, Text, Label, Tk, END, Menu, PhotoImage, Canvas
from tkinter.messagebox import showinfo, showwarning
from words.strings import check_string, count_words, count_chars

default_sentence = "In a hole in the ground there lived a hobbit. Not " \
    "a nasty, dirty, wet hole, filled with the ends of " \
    "worms and an oozy smell, nor yet a dry, bare, " \
    "sandy hole with nothing in it to sit down on or to " \
    "eat: it was a hobbit-hole, and that means " \
    "comfort."

color_blue = "#2980b9"
color_lightblue = "#3498db"
color_white = "#ecf0f1"
color_black = "#2c3e50"
color_grey = "#7f8c8d"
color_lightgrey = "#95a5a6"
color_red = "#c0392b"
color_lightred = "#e74c3c"


def get_textarea_content():
    """
    Return text from textarea component striped
    """
    return textarea.get("0.0", END).strip()


def check_textarea_content():
    """
    Check textarea_content and raise a warning if needed
    :return: if texterea contains a valid string
    :rtype: bool 
    """
    string = get_textarea_content()
    if len(string) == 0:
        showwarning(title="Ooopsii", message="Please provide a text !")
        return False
    return True


def callback_chars():
    """
    Callback to count chars in given sentence
    """
    if check_textarea_content():
        string = get_textarea_content()
        showinfo(title="About your text",
                 message="Your text is %s chars long." % count_chars(string))


def callback_words():
    """
    Callback to count words in given sentence
    """
    if check_textarea_content():
        string = get_textarea_content()
        showinfo(title="About your text",
                 message="Your text is %s words long." % count_words(string))


def callback_about(event):
    """
    Show about page
    """
    showinfo(title="About",
             message="A simple application made by DelahayeYourself.")


if __name__ == '__main__':
    """
    World famous words counter

    1. Create frame and configure it
    2. Grid configuration
    3. Menu configuration
    4. Element configuration
    """
    frame = Tk()
    frame.title("World famous words counter !")
    icon = PhotoImage(file='icon.png')
    frame.iconphoto(False, icon)
    frame.minsize(500, 450)

    frame.rowconfigure(1, weight=1)
    frame.columnconfigure(0, weight=1)
    frame.columnconfigure(1, weight=1)
    frame.columnconfigure(2, weight=1)

    menu = Menu(frame)
    file_menu = Menu(menu, tearoff=0)
    file_menu.add_command(label='Count words', command=callback_words)
    file_menu.add_command(label='Count characters', command=callback_chars)
    file_menu.add_separator()
    file_menu.add_command(label='Quit', command=frame.quit)
    menu.add_cascade(label='File', menu=file_menu)
    frame.config(menu=menu)

    logobanner = Canvas(frame,
                        width=300,
                        height=59,
                        bd=0,
                        highlightthickness=0,
                        relief='ridge')
    logobanner.bind("<Button-1>", callback_about)
    banner = PhotoImage(file='logo.png')
    logobanner.grid(row=0, columnspan=3, pady=10)
    logobanner.create_image(0, 0, image=banner, anchor='nw')

    textarea = Text(frame,
                    wrap='word',
                    bg=color_white,
                    fg=color_black)
    textarea.insert(END, default_sentence)
    textarea.grid(row=1, columnspan=3, padx=10, pady=10, sticky='nsew')

    btn_words_count = Button(frame,
                             image=icon,
                             compound="left",
                             text='Count words',
                             command=callback_words,
                             bg=color_blue,
                             activebackground=color_lightblue)
    btn_words_count.grid(row=2, column=0)

    btn_chars_count = Button(frame,
                             image=icon,
                             compound="left",
                             text='Count characters',
                             command=callback_chars,
                             bg=color_grey,
                             activebackground=color_lightgrey,
                             fg=color_black,
                             activeforeground=color_black)
    btn_chars_count.grid(row=2, column=1)

    btn_quit = Button(frame,
                      text="Exit",
                      command=frame.quit,
                      bg=color_red,
                      activebackground=color_lightred,
                      fg=color_black,
                      activeforeground=color_black)
    btn_quit.grid(row=2, column=2)

    frame.mainloop()
